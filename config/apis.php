<?php

return [
    'enabled' => [
        \Hellgrau\Adapter\IfcApi::class
    ],
    'available' => [
        \Hellgrau\Adapter\ExampleApi::class
    ]
];
