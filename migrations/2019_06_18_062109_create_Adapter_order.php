<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdapterOrder extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('adapter_order')) {
            Schema::create('adapter_order', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('api_id');
                $table->integer('external_id');
                $table->longText('input');
                $table->longText('output');
                $table->dateTime('synchronized_at')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
