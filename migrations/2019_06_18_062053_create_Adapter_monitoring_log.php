<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdapterMonitoringLog extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('adapter_monitoring_log')) {
            Schema::create('adapter_monitoring_log', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('api_id')->nullable();
                $table->enum('type', ['ERROR', 'INFO']);
                $table->string('message');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monitoring_log');
    }
}
