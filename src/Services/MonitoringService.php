<?php

namespace Hellgrau\Adapter\Services;

use Hellgrau\Adapter\Models\Api;
use Hellgrau\Adapter\Models\MonitoringLog;
use Hellgrau\Adapter\Models\Order;

class MonitoringService
{
    public static function logApiError($message, Api $api = null)
    {
        $log = new MonitoringLog();
        $log->message = $message;
        $log->type = 'ERROR';
        $log->api_id = $api ? $api->id : null;
        $log->save();
    }

    public static function logOrderPush(Order $order)
    {
        $log = new MonitoringLog();
        $log->message = 'Order ' . $order->id . ' wurde synchronisiert.';
        $log->type = 'INFO';
        $log->api_id = null;
        $log->save();
    }
}
