<?php

namespace Hellgrau\Adapter;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Hellgrau\Adapter\Models\Order;
use Hellgrau\Adapter\Services\MonitoringService;

class PushService
{
    public static function sendOrder(Order $order)
    {
        $client = new Client();
        $response = $client->post(env('ADAPTER_PUSH_URL'), [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ],
            'auth' => [
                env('ADAPTER_PUSH_USERNAME'),
                env('ADAPTER_PUSH_PASSWORD'),
            ],
            'body' => json_encode([
                'external_id' => $order->external_id,
                'properties' => json_decode($order->output, true)
            ]),
            'verify' => false
        ]);

        $order->synchronized_at = Carbon::now();
        $order->save();

        MonitoringService::logOrderPush($order);
    }
}
