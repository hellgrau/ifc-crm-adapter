<?php

namespace Hellgrau\Adapter;

class ApiService
{
    public static function getEnabledApis()
    {
        return config('adapter.apis.enabled');
    }
}
