<?php

namespace Hellgrau\Adapter;

use Hellgrau\Adapter\Jobs\PushOrder;
use Hellgrau\Adapter\Models\Api;
use Hellgrau\Adapter\Models\Order;
use Hellgrau\Adapter\Services\MonitoringService;
use Illuminate\Support\Facades\Log;

class PullService
{
    public static function crawlApis()
    {
        $apis = ApiService::getEnabledApis();

        foreach ($apis as $api) {
            $instance = new $api;

            if (!$api = Api::where('name', $instance->getName())->first()) {
                MonitoringService::logApiError('unknown api (' . $instance->getName() . ')');
                continue;
            }

            if (!$instance->login()) {
                MonitoringService::logApiError('login failed', $api);
                continue;
            }

            if (!$data = $instance->getData()) {
                Log::info(json_encode($data));
                MonitoringService::logApiError('could not request data', $api);
                continue;
            }

            foreach ($data['data'] as $row) {

                if ($row->type == 'baufi') {
                    $item = $instance->mapItem($row);

                    $order = new Order();
                    $order->api_id = $api->id;
                    $order->external_id = $item['external_id'];
                    $order->input = json_encode($row);
                    $order->output = json_encode($item);
                    $order->created_at = $item['created_at'] ?? date('Y-m-d H:i:s');
                    $order->updated_at = $item['updated_at'] ?? date('Y-m-d H:i:s');
                    $order->save(['timestamps' => false]);

                    if (!empty($item['vorgang.antragsteller.0.telefonnummer'] ?? null)
                        || !empty($item['vorgang.antragsteller.0.email'] ?? null)
                        || !empty($item['vorgang.antragsteller.0.telefonnummer_beruf'] ?? null)
                        || !empty($item['vorgang.antragsteller.0.mobilfunknummer'] ?? null)
                    ) {
                        PushOrder::dispatch($order);
                    }
                }
            }
        }
    }
}
