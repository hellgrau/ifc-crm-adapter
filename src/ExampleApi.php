<?php

namespace Hellgrau\Adapter;

use Exception;
use GuzzleHttp\Client;
use Hellgrau\Adapter\Interfaces\ApiInterface;

class ExampleApi implements ApiInterface
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'Example API';
    }

    /**
     * @return bool
     */
    public function login(): bool
    {
        return true;
    }

    /**
     * @return array
     * @throws ApiRequestException
     */
    public function getData(): array
    {
        $client = new Client();
        $response = $client->get('https://jsonplaceholder.typicode.com/posts');

        if ($response->getStatusCode() == 200) {
            try {
                $body = $response->getBody()->getContents();
                $json = (array)json_decode($body);
            } catch (Exception $exception) {
                throw new ApiRequestException('Could not fetch data from API ' . $this->getName(), $response->getStatusCode());
            }

            return $json;
        }

        return [];
    }

    /**
     * @param array $item
     * @return array
     */
    public function mapItem($item): array
    {
        return [
            'post.externe_id' => $item->id ?? '',
            'post.user_id' => $item->userId ?? '',
            'post.title' => $item->title ?? '',
            'post.body' => $item->body ?? '',
        ];
    }
}
