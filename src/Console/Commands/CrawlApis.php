<?php

namespace Hellgrau\Adapter\Console\Commands;

use Hellgrau\Adapter\PullService;
use Illuminate\Console\Command;

class CrawlApis extends Command
{
    protected $signature = 'adapter:crawl-apis';
    protected $description = 'Crawl enabled APIs';

    public function handle()
    {
        PullService::crawlApis();
    }
}
