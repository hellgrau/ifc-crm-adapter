<?php

namespace Hellgrau\Adapter\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $api_id
 * @property int $external_id
 * @property string $input
 * @property string $output
 * @property Carbon|null $synchronized_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Order extends Model
{
    protected $table = 'adapter_order';
}
