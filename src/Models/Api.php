<?php

namespace Hellgrau\Adapter\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $token
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Api extends Model
{
    protected $table = 'adapter_api';
}
