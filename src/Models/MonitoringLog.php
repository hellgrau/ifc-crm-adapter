<?php

namespace Hellgrau\Adapter\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int|null $api_id
 * @property string $type (ERROR, INFO)
 * @property string $message
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class MonitoringLog extends Model
{
    protected $table = 'adapter_monitoring_log';
}
