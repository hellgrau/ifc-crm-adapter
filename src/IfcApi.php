<?php

namespace Hellgrau\Adapter;

use Exception;
use GuzzleHttp\Client;
use Hellgrau\Adapter\Interfaces\ApiInterface;
use Hellgrau\Adapter\Models\Order;

class IfcApi implements ApiInterface
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'immo-finanzcheck.de';
    }

    /**
     * @return bool
     */
    public function login(): bool
    {
        return true;
    }

    /**
     * @return array
     * @throws ApiRequestException
     */
    public function getData(): array
    {
        try {
            $client = new Client();
            $response = $client->get(env('ADAPTER_PULL_IFC_URL') . '?max_id=' . (int)Order::where('api_id', 1)->max('external_id'), [
                'headers' => [
                    'Authorization' => 'Bearer ' . env('ADAPTER_PULL_IFC_TOKEN'),
                    'Accept' => 'application/json',
                ],
                'proxy' => env('ADAPTER_PROXY')
            ]);
        } catch (Exception $exception) {
            report($exception);
            throw new ApiRequestException('Could not fetch data from API ' . $this->getName(), 0);
        }

        if ($response->getStatusCode() == 200) {
            try {
                $body = $response->getBody()->getContents();
                $json = (array)json_decode($body);
            } catch (Exception $exception) {
                report($exception);
                throw new ApiRequestException('Could not fetch data from API ' . $this->getName(), $response->getStatusCode());
            }

            return $json;
        }

        return [];
    }

    /**
     * @param array $item
     * @return array
     * @throws ApiUnknownException
     */
    public function mapItem($item): array
    {
        switch ($item->type) {
            case 'finanzierung':
                $return = $this->mapTypeFunding($item);
                break;

            case 'baufi':
                $return = $this->mapTypeFinancing($item);
                break;

            case 'kontakt':
            case 'rueckruf':
                $return = $this->mapTypeContact($item);
                break;

            default:
                $return = [];
        }

        $return['external_id'] = $item->id;
        $form_id = $item->json_data->form_id ?? null;
        if($form_id !=null )
        {
            $return['vorgang.quelle'] = $form_id;
        }

        return $return;
    }

    /**
     * @param $item
     * @return array
     */
    private function mapTypeFunding($item)
    {
        $json = $this->transformBasics($item->json_data);
        $json->anrede = strtolower($json->anrede ?? null);
        $json->KUNDE1_GEBURTSDATUM = \DateTime::createFromFormat('d.m.Y', $json->KUNDE1_GEBURTSDATUM ?? null) ? $json->KUNDE1_GEBURTSDATUM : null;
        $json->KUNDE2_GEBURTSDATUM = \DateTime::createFromFormat('d.m.Y', $json->KUNDE2_GEBURTSDATUM ?? null) ? $json->KUNDE2_GEBURTSDATUM : null;
        $json->geburtsdatum = \DateTime::createFromFormat('d.m.Y', $json->geburtsdatum ?? null) ? $json->geburtsdatum : null;
        $json->anstellungsVerhaeltnis = $this->setJobType($json->anstellungsVerhaeltnis ?? null);
        $json->immobilieGefunden = ($json->immobilieGefunden ?? '') == 'Ja' ? 1 : 0;

        $mapping = [
            'tracking_id' => 'vorgang.tracking.tracking_id',
            'utm_campaign' => 'vorgang.tracking.utm_campaign',
            'utm_content' => 'vorgang.tracking.utm_content',
            'utm_medium' => 'vorgang.tracking.utm_medium',
            'utm_source' => 'vorgang.tracking.utm_source',
            'utm_term' => 'vorgang.tracking.utm_term',
            'BUNDESLAND' => 'vorgang.antragsteller.0.bundesland',
            'DARLEHENS_SUMME' => 'vorgang.vorhaben.0.gewuenschte_darlehenssumme',
            'GRUNDSCHULD' => 'vorgang.grundschuld', // NEW
            'IMMOBILIE_BAUJAHR' => 'vorgang.immobilien.0.baujahr',
            'IMMOBILIE_GRUNDSTUECKSFLAECHE' => 'vorgang.immobilien.0.grundstuecksflaeche',
            'IMMOBILIE_KOSTEN' => 'vorgang.immobilien.0.kaufpreis',
            'IMMOBILIE_ORT' => 'vorgang.immobilien.0.adresse.ort',
            'IMMOBILIE_PLZ' => 'vorgang.immobilien.0.adresse.plz',
            'IMMOBILIE_STRASSE_MIT_HAUSNUMMER' => 'vorgang.immobilien.0.adresse.strasse', // Hausnummer parsen
            'IMMOBILIE_WOHNFLAECHE' => 'vorgang.immobilien.0.wohnflaeche',
            'KAUFPREIS' => 'vorgang.immobilien.0.kaufpreis',
            'KAUFPREIS_GRUNDSTUECK' => 'vorgang.immobilien.0.kaufpreis_grundstueck', // NEW
            'KUNDE1_ALTERNATIVE_TELEFON' => 'vorgang.antragsteller.0.telefonnummer_alternativ', // NEW
            'KUNDE1_AUSGABEN' => 'vorgang.ausgaben.sonstiges.0.betrag',
            'KUNDE1_EIGENKAPITAL' => 'vorgang.antragsteller.0.eigenkapital', // NEW
            'KUNDE1_EINKOMMEN' => 'vorgang.haushaltsnettoeinkommen',
            'KUNDE1_EMAIL' => 'vorgang.antragsteller.0.email',
            'KUNDE1_GEBURTSDATUM' => 'vorgang.antragsteller.0.geburtsdatum',
            'KUNDE1_MONATLICHE_RATE' => 'vorgang.wuensche_und_ziele.wunschrate',
            'KUNDE1_NACHNAME' => 'vorgang.antragsteller.0.nachname',
            'KUNDE1_ORT' => 'vorgang.antragsteller.0.anschrift.ort',
            'KUNDE1_PLZ' => 'vorgang.antragsteller.0.anschrift.plz',
            'KUNDE1_STRASSE_MIT_HAUSNUMMER' => 'vorgang.antragsteller.0.anschrift.strasse', // Hausnummer parsen
            'KUNDE1_TELEFON' => 'vorgang.antragsteller.0.telefonnummer',
            'KUNDE1_VORNAME' => 'vorgang.antragsteller.0.vorname',
            'KUNDE2_EINKOMMEN' => null,
            'KUNDE2_GEBURTSDATUM' => 'vorgang.antragsteller.1.geburtsdatum',
            'KUNDE2_NACHNAME' => 'vorgang.antragsteller.1.nachname',
            'KUNDE2_VORNAME' => 'vorgang.antragsteller.1.vorname',
            'MITTEILUNG_KUNDE' => 'vorgang.leadformular.kommentar',
            'TILGUNG' => 'vorgang.wuensche_und_ziele.tilgung', //new
            'UMSCHULDUNGS_DATUM' => 'vorgang.umschuldungsdatum',
            'abloesedatum' => 'vorgang.wuensche_und_ziele.abloesedatum', // NEW
            'advisor' => 'vorgang.wuensche_und_ziele.wunsch_berater', // new
            'anrede' => 'vorgang.antragsteller.0.anrede',
            'anschaffungsKosten' => 'vorgang.immobilien.0.kaufpreis',
            'anstellungsVerhaeltnis' => 'vorgang.bonitaet.gehalt.0.beschaeftigung',
            'aussenanlage' => 'vorgang.immobilien.0.aussenanlagen',
            'ausstattung' => 'vorgang.immobilien.0.ausstattung',
            'baujahr' => 'vorgang.immobilien.0.baujahr',
            'bauvolumen' => 'vorgang.immobilien.0.bauvolumn',
            'bauweise' => 'vorgang.immobilien.0.bauweise',
            'bodenrichtwert' => 'vorgang.immobilien.0.bodenrichtwert',
            'bruttogrundflaeche' => 'vorgang.immobilien.0.bruttogrundflaeche',
            'carports' => 'vorgang.immobilien.0.carports',
            'cd.buttonFeatures' => null,
            'cd.buttonText' => null,
            'cd.formFeatures' => null,
            'comments' => 'vorgang.leadformular.kommentar',
            'darlehensSumme' => 'vorgang.vorhaben.0.gewuenschte_darlehenssumme',
            'dl' => null,
            'e' => null,
            'ec' => null,
            'email' => 'vorgang.antragsteller.0.email',
            'es' => null,
            'ev' => null,
            'exp' => null,
            'finanzierungsZweck' => 'vorgang.vorhaben.0.vorhaben',
            'finanzierungszweck' => 'vorgang.vorhaben.0.vorhaben',
            'form_id' => 'vorgang.leadformular.id',
            'free' => null,
            'garagen' => 'vorgang.immobilien.0.garagen',
            'gebaeudetyp' => 'vorgang.immobilien.0.gebaeudetyp',
            'geburtsdatum' => 'vorgang.antragsteller.0.geburtsdatum',
            'geschosse' => 'vorgang.immobilien.0.geschosse',
            'grundstuecksflaeche' => 'vorgang.immobilien.0grundstuecksflaeche',
            'haushaltsnettoeinkommen' => 'vorgang.haushaltsnettoeinkommen',
            'hausnummer' => 'vorgang.antragsteller.0.anschrift.hausnummer',
            'id' => null,
            'if' => null,
            'immobilieGefunden' => 'vorgang.hasimmobilie',
            'immobilieZeitpunkt' => 'vorgang.immobilien.0.zeitpunkt',
            'it' => null,
            'kaufgrund' => 'vorgang.immobilien.0.kaufgrund',
            'keller' => 'vorgang.immobilien.0.keller',
            'kostenlose_bewertung' => 'vorgang.immobilien.0.kostenlose_bewertung',
            'kunde_anrede' => 'vorgang.antragsteller.0.anrede',
            'kunde_email' => 'vorgang.antragsteller.0.email',
            'kunde_hausnummer' => 'vorgang.antragsteller.0.anschrift.hausnummer',
            'kunde_nachname' => 'vorgang.antragsteller.0.nachname',
            'kunde_ort' => 'vorgang.antragsteller.0.anschrift.ort',
            'kunde_plz' => 'vorgang.antragsteller.0.anschrift.plz',
            'kunde_strasse' => 'vorgang.antragsteller.0.anschrift.strasse',
            'kunde_telefon' => 'vorgang.antragsteller.0.telefonnummer',
            'kunde_vorname' => 'vorgang.antragsteller.0.vorname',
            'leadqualitaet' => 'vorgang.leadqualitaet',

            'mieteinnahmen' => 'vorgang.immobilien.0.mieteinnahmen_netto_kalt_monatlich',
            'modernisierung' => 'vorgang.immobilien.0.modernisierung', // NEW
            'modernisierungsjahr' => 'vorgang.immobilien.0.modernisierungsjahr',
            'modernisierungskosten' => 'vorgang.immobilien.0.modernisierungskosten',
            'nachname' => 'vorgang.antragsteller.0.nachname',
            'nettokaltmiete' => 'vorgang.immobilien.0.nettokaltmiete',
            'nhk2010_ausstattung' => 'vorgang.immobilien.0.nhk2010_ausstattung',
            'nhk2010_dach' => 'vorgang.immobilien.0.nhk2010_dach',
            'nhk2010_typ' => 'vorgang.immobilien.0.nhk2010_typ',
            'nutzung' => 'vorgang.immobilien.0.nutzungsart',
            'nutzungsArt' => 'vorgang.immobilien.0.nutzungsart',
            'nutzungsArt.0' => 'vorgang.immobilien.0.nutzungsart',
            'nutzungsArt.1' => null,
            'o' => null,
            'objektArt' => 'vorgang.immobilien.0.art',
            'objekt_plz' => 'vorgang.immobilien.0.adresse.plz',
            'objektart' => 'vorgang.immobilien.0.art',
            'ort' => 'vorgang.antragsteller.0.anschrift.ort',
            'p' => null,
            'pkwep' => 'vorgang.immobilien.0.pkwep',
            'plz' => 'vorgang.antragsteller.0.anschrift.plz',
            'r' => null,
            'rl' => null,
            'rs' => null,
            'sh' => null,
            'strasse' => 'vorgang.antragsteller.0.anschrift.strasse',
            'sw' => null,
            'telefon' => 'vorgang.antragsteller.0.telefonnummer',
            'tilgung' => 'vorgang.wuensche_und_ziele.tilgung', //new
            'titel_dr' => null,
            'titel_prof' => null,
            'titel' => 'vorgang.antragsteller.0.titel',
            'ts' => null,
            'ttf' => null,
            'tts' => null,
            'ttse' => null,
            'type' => null,
            'urbanitaet' => 'vorgang.urbanitaet',
            'v' => null,
            'vorname' => 'vorgang.antragsteller.0.vorname',
            'weitere_angaben' => null,
            'weitere_informationen' => null,
            'weitererAntragssteller' => null,
            'widerrufsfrist' => 'vorgang.leadformular.widerrufsfrist_akzeptiert',
            'wohneinheiten' => 'vorgang.immobilien.0.wohneinheiten',
            'wohnflaeche' => 'vorgang.immobilien.0.wohnflaeche',
            'wohnlage' => 'vorgang.immobilien.0.wohnlage',
            'wohnraum_eingabe' => 'vorgang.immobilien.0.wohnraum_eingabe',
            'wunschrate' => 'vorgang.wuensche_und_ziele.wunschrate',
            'zinsbindung' => 'vorgang.wuensche_und_ziele.zinsbindung_in_jahren'
        ];

        return $this->replace($json, $mapping);
    }

    /**
     * @param $item
     * @return array
     */
    private function mapTypeFinancing($item)
    {
        $json = $item->json_data;
        $json->quelle = 'vergleich.de';
        $json->geburtsdatum = \DateTime::createFromFormat('d.m.Y', $json->Kundendaten->Person[0]->Persondetails->Geburtsdatum ?? null) ? $json->Kundendaten->Person[0]->Persondetails->Geburtsdatum : null;

        $json->DARLEHENS_SUMME = $json->Vertragsdaten->Vertrag->Finanzierung->Darlehensbetrag ?? null;
        $finanzierungszweck = $json->Vertragsdaten->Vertrag->Finanzierungszweck ?? null;

        if ($finanzierungszweck != null) {
            $json->FINANZIERUNGSZWECK = $this->setVergleichDeFinanzierungszweck($finanzierungszweck);
        }

        if( $json->FINANZIERUNGSZWECK ==='kauf'
            ||  $json->FINANZIERUNGSZWECK ==='kauf_neubau_bautraeger'
            ||  $json->FINANZIERUNGSZWECK ==='kapitalbeschaffung')
        {
            $json->KAUFPREIS = $json->Vertragsdaten->Vertrag->Objekt->Kaufpreis ?? null;
        } else if( $json->FINANZIERUNGSZWECK ==='anschlussfinanzierung'
            ||  $json->FINANZIERUNGSZWECK ==='modernisierung'
            ||  $json->FINANZIERUNGSZWECK ==='umbau'
            ||  $json->FINANZIERUNGSZWECK ==='anbau')
        {
            $json->OBJEKTWERT = $json->Vertragsdaten->Vertrag->Objekt->Kaufpreis ?? null;
        }



        $json->KUNDE1_MONATLICHE_RATE = $json->Vertragsdaten->Vertrag->Finanzierung->gewuenschte_monatliche_Rate_Tilgung ?? null;
        $json->ZINSBINDUNG_IN_JAHREN = $json->Vertragsdaten->Vertrag->Finanzierung->Zinsfestschreibung ?? null;
        $json->UMSCHULDUNGS_DATUM = \DateTime::createFromFormat('d.m.Y', $json->Vertragsdaten->Vertrag->Finanzierung->Auszahlungs_Datum ?? null) ? $json->Vertragsdaten->Vertrag->Finanzierung->Auszahlungs_Datum : null;
        $json->abloesedatum = \DateTime::createFromFormat('d.m.Y', $json->Vertragsdaten->Vertrag->Finanzierung->Auszahlungs_Datum ?? null) ? $json->Vertragsdaten->Vertrag->Finanzierung->Auszahlungs_Datum : null;


        $json->IMMOBILIE_BAUJAHR = $json->Vertragsdaten->Vertrag->Objekt->Baujahr ?? null;
        $immobilienart = $json->Vertragsdaten->Vertrag->Objekt->Objektart ?? null;

        if ($immobilienart != null) {
            $json->IMMOBILIE_ART = $this->setVergleichDeObjektArt($immobilienart);
        }

        $immobilienutzungsart = $json->Vertragsdaten->Vertrag->Objekt->Nutzung_Immobilie ?? null;
        if ($immobilienutzungsart != null) {
            $json->IMMOBILIE_NUTZUNGSART = $this->setVergleichDeObjektNutzungsart($immobilienutzungsart);
        }

        $json->IMMOBILIE_GRUNDSTUECKSFLAECHE = $json->Vertragsdaten->Vertrag->Objekt->Grundstuecksgroesse ?? null;
        $json->IMMOBILIE_ORT = $json->Vertragsdaten->Vertrag->Objekt->Adresse->Ort ?? null;
        $json->IMMOBILIE_PLZ = $json->Vertragsdaten->Vertrag->Objekt->Adresse->Postleitzahl ?? null;
        $json->IMMOBILIE_STRASSE_MIT_HAUSNUMMER = $json->Vertragsdaten->Vertrag->Objekt->Adresse->Strasse ?? null;
        $json->IMMOBILIE_WOHNFLAECHE = $json->Vertragsdaten->Vertrag->Objekt->Wohnflaeche_gesamt ?? null;


        $json->KAUFPREIS_GRUNDSTUECK = $json->Vertragsdaten->Vertrag->Objekt->Kaufpreis_Grundstueck ?? null;
        $json->KUNDE1_ALTERNATIVE_TELEFON = $json->Kundendaten->Person[0]->Kontaktdaten->Telefon_Privat ?? null;
        $json->KUNDE1_AUSGABEN = $json->Kundendaten->Person[0]->Finanzsituation->Ausgaben->Warmmiete ?? null;
        $json->KUNDE1_EIGENKAPITAL = $json->Kundendaten->Person[0]->Finanzsituation->Vermoegen->Eigenkapital ?? null;
        $json->KUNDE1_EINKOMMEN = $json->Kundendaten->Person[0]->Finanzsituation->Einnahmen->Gehalt ?? null;
        $json->KUNDE1_EINKOMMEN_B = $json->Kundendaten->Person[0]->Finanzsituation->Einnahmen->Gehalt ?? null;

        $json->KUNDE1_EMAIL = $json->Kundendaten->Person[0]->Kontaktdaten->Email ?? null;

        $kunde1_geburtsdatum = $json->Kundendaten->Person[0]->Persondetails->Geburtsdatum ;

        if($kunde1_geburtsdatum != null){
            $json->KUNDE1_GEBURTSDATUM = \DateTime::createFromFormat('Y-m-d\TH:i:s', $kunde1_geburtsdatum)->format('d.m.Y');
        }

        $kunde2_geburtsdatum = $json->Kundendaten->Person[1]->Persondetails->Geburtsdatum ;

        if($kunde2_geburtsdatum != null){
            $json->KUNDE2_GEBURTSDATUM = \DateTime::createFromFormat('Y-m-d\TH:i:s', $kunde2_geburtsdatum)->format('d.m.Y');
        }

        $json->KUNDE1_NACHNAME = $json->Kundendaten->Person[0]->Persondetails->Nachname ?? null;
        $json->KUNDE1_VORNAME = $json->Kundendaten->Person[0]->Persondetails->Vorname ?? null;
        $json->KUNDE1_ORT = $json->Kundendaten->Person[0]->Wohndaten->aktuell->Adresse->Ort ?? null;
        $json->KUNDE1_PLZ = $json->Kundendaten->Person[0]->Wohndaten->aktuell->Adresse->Postleitzahl ?? null;
        $json->KUNDE1_STRASSE_MIT_HAUSNUMMER = $json->Kundendaten->Person[0]->Wohndaten->aktuell->Adresse->Strasse ?? null;
        $json->KUNDE1_TELEFON = $json->Kundendaten->Person[0]->Kontaktdaten->Telefon_Privat ?? null;
        $json->KUNDE1_TELEFONE_BERUF = $json->Kundendaten->Person[0]->Kontaktdaten->Telefon_Beruf ?? null;
        $json->KUNDE1_MOBIL = $json->Kundendaten->Person[0]->Kontaktdaten->Telefon_Mobil ?? null;

        $json->KUNDE2_NACHNAME = $json->Kundendaten->Person[1]->Persondetails->Nachname ?? null;
        $json->KUNDE2_VORNAME = $json->Kundendaten->Person[1]->Persondetails->Vorname ?? null;

        $json->KUNDE2_TELEFON = $json->Kundendaten->Person[1]->Kontaktdaten->Telefon_Privat ?? null;
        $json->KUNDE2_TELEFONE_BERUF = $json->Kundendaten->Person[1]->Kontaktdaten->Telefon_Beruf ?? null;
        $json->KUNDE2_MOBIL = $json->Kundendaten->Person[1]->Kontaktdaten->Telefon_Mobil ?? null;

        $immobilieGefunden = $json->Vertragsdaten->Vertrag->Bemerkung;
        if($immobilieGefunden != null){
            if(preg_match('/immobilie_gefunden = ja/', $immobilieGefunden)) {
                $json->IMMOBILIE_GEFUNDEN = '1';
            }
        }

        $json->MITTEILUNG_KUNDE = $json->Vertragsdaten->Vertrag->Bemerkung ?? null;
        $json->anrede = strtolower($json->Kundendaten->Person[0]->Persondetails->Anrede ?? null);

        $anstellungsveraeltnis = $json->Kundendaten->Person[0]->Beruf->Beschaeftigungsverhaeltnis ?? null;

        if ($anstellungsveraeltnis != null)
        {
            $json->anstellungsVerhaeltnis = $this->setVergleichDeAnstellungsverhaeltnis($anstellungsveraeltnis);
        }



        $mapping = [
            'quelle' => 'vorgang.quelle',
            'tracking_id' => 'vorgang.trackingtracking.tracking_id',
            'utm_campaign' => 'vorgang.tracking.utm_campaign',
            'utm_content' => 'vorgang.tracking.utm_content',
            'utm_medium' => 'vorgang.tracking.utm_medium',
            'utm_source' => 'vorgang.tracking.utm_source',
            'utm_term' => 'vorgang.tracking.utm_term',


            'ZINSBINDUNG_IN_JAHREN' =>'vorgang.wuensche_und_ziele.zinsbindung_in_jahren',
            'FINANZIERUNGSZWECK' => 'vorgang.vorhaben.0.vorhaben',

            'DARLEHENS_SUMME' => 'vorgang.vorhaben.0.gewuenschte_darlehenssumme',
            'IMMOBILIE_BAUJAHR' => 'vorgang.immobilien.0.baujahr',
            'IMMOBILIE_GEFUNDEN'=> 'vorgang.hasimmobilie',
            'IMMOBILIE_ART' => 'vorgang.immobilien.0.art',
            'IMMOBILIE_GRUNDSTUECKSFLAECHE' => 'vorgang.immobilien.0.grundstuecksflaeche',
            'IMMOBILIE_ORT' => 'vorgang.immobilien.0.adresse.ort',
            'IMMOBILIE_PLZ' => 'vorgang.immobilien.0.adresse.plz',
            'IMMOBILIE_STRASSE_MIT_HAUSNUMMER' => 'vorgang.immobilien.0.adresse.strasse', // Hausnummer parsen
            'IMMOBILIE_WOHNFLAECHE' => 'vorgang.immobilien.0.wohnflaeche',
            'IMMOBILIE_NUTZUNGSART' => 'vorgang.immobilien.0.nutzungsart',
            'KAUFPREIS' => 'vorgang.immobilien.0.kaufpreis',
            'OBJEKTWERT' => 'vorgang.immobilien.0.objektwert',
            'KAUFPREIS_GRUNDSTUECK' => 'vorgang.immobilien.0.kaufpreis_grundstueck', // NEW
            'KUNDE1_ALTERNATIVE_TELEFON' => 'vorgang.antragsteller.0.telefonnummer_alternativ', // NEW
            'KUNDE1_AUSGABEN' => 'vorgang.ausgaben.sonstiges.0.betrag',
            'KUNDE1_EIGENKAPITAL' => 'vorgang.antragsteller.0.eigenkapital', // NEW
            'KUNDE1_EINKOMMEN' => 'vorgang.haushaltsnettoeinkommen',
            'KUNDE1_EINKOMMEN_B' => 'vorgang.bonitaet.gehalt.0.betrag',
            'KUNDE1_EMAIL' => 'vorgang.antragsteller.0.email',
            'KUNDE1_GEBURTSDATUM' => 'vorgang.antragsteller.0.geburtsdatum',
            'KUNDE1_MONATLICHE_RATE' => 'vorgang.wuensche_und_ziele.wunschrate',
            'KUNDE1_NACHNAME' => 'vorgang.antragsteller.0.nachname',
            'KUNDE1_ORT' => 'vorgang.antragsteller.0.anschrift.ort',
            'KUNDE1_PLZ' => 'vorgang.antragsteller.0.anschrift.plz',
            'KUNDE1_STRASSE_MIT_HAUSNUMMER' => 'vorgang.antragsteller.0.anschrift.strasse', // Hausnummer parsen
            'KUNDE1_TELEFON' => 'vorgang.antragsteller.0.telefonnummer',
            'KUNDE1_MOBIL' => 'vorgang.antragsteller.0.mobilfunknummer',
            'KUNDE1_TELEFONE_BERUF' => 'vorgang.antragsteller.0.telefonnummer_beruf',
            'KUNDE1_VORNAME' => 'vorgang.antragsteller.0.vorname',

            'KUNDE2_EINKOMMEN' => null,
            'KUNDE2_GEBURTSDATUM' => 'vorgang.antragsteller.1.geburtsdatum',
            'KUNDE2_NACHNAME' => 'vorgang.antragsteller.1.nachname',
            'KUNDE2_VORNAME' => 'vorgang.antragsteller.1.vorname',
            'KUNDE2_TELEFON' => 'vorgang.antragsteller.1.telefonnummer',
            'KUNDE2_MOBIL' => 'vorgang.antragsteller.1.mobilfunknummer',
            'KUNDE2_TELEFONE_BERUF' => 'vorgang.antragsteller.1.telefonnummer_beruf',

            'MITTEILUNG_KUNDE' => 'vorgang.leadformular.kommentar',
            'TILGUNG' => 'vorgang.wuensche_und_ziele.tilgung', //new
            'UMSCHULDUNGS_DATUM' => 'vorgang.umschuldungsdatum',
            'abloesedatum' => 'vorgang.wuensche_und_ziele.abloesedatum', // NEW
            'advisor' => 'vorgang.wuensche_und_ziele.wunsch_berater', // new
            'anrede' => 'vorgang.antragsteller.0.anrede',

            'anstellungsVerhaeltnis' => 'vorgang.bonitaet.gehalt.0.beschaeftigung',
        ];

        return $this->replace($json, $mapping);
    }

    private function setVergleichDeAnstellungsverhaeltnis($value){

        $employmentStatus = [
            'Arbeitnehmer' => 'angestellter',
            //'' => 'arbeiter',
            'Beamter' => 'beamter',
            //'' => 'freiberufler',
            //'' => 'selbstaendiger',
            //'' => 'hausfrau',
            //'' => 'arbeitsloser',
            //'' => 'rentner',
            //'' => 'elternzeit',
            //'' => 'soldat',
            //'' => 'student',
            //'' => 'gewerbetreibender',
            'Geschäftsführender Gesellschafter' => 'geschaeftsfuehrender_gesellschafter'
        ];

        if (key_exists($value, $employmentStatus)) {
            return $employmentStatus[$value];
        }

        return $value;
    }

    private function setVergleichDeFinanzierungszweck($value)
    {
        $finanzierungszweck = [
            'Kauf' => 'kauf',
            'An-/Umbau' => 'anbau',
            'Neubau' => 'neubau',
            'Umschuldung' => 'anschlussfinanzierung',
            //''=>'kauf_neubau_bautraeger',
            //''=>'modernisierung',
            //''=>'umbau',
            //''=>'kapitalbeschaffung',
        ];

        if (key_exists($value, $finanzierungszweck)) {
            return $finanzierungszweck[$value];
        }
        return $value;
    }

    private function setVergleichDeObjektNutzungsart($value){
        $nutzungImmobilie = [
            'Eigennutzung' => 'eigengenutzt',
            'Vermietung' => 'vermietet',
            //''=>'teilvermietet',
        ];
        if (key_exists($value, $nutzungImmobilie)) {
            return $nutzungImmobilie[$value];
        }
        return $value;
    }

    private function setVergleichDeObjektArt($value){
        $objektArt = [
            'Einfamilienhaus' => 'einfamilienhaus',
            'Mehrfamilienhaus' => 'mehrfamilienhaus',
            'Eigentumswohnung' => 'eigentumswohnung',
            //''=>'zweifamilienhaus',
            //''=>'reihenhaus',
            //''=>'grundstueck',
        ];

        if (key_exists($value, $objektArt)) {
            return $objektArt[$value];
        }
        return $value;
    }

    /**
     * @param $item
     * @return array
     */
    private function mapTypeContact($item)
    {
        $json = $this->transformBasics($item->json_data);
        $json->anrede = strtolower($json->anrede ?? null);
        $json->geburtsdatum = \DateTime::createFromFormat('d.m.Y', $json->geburtsdatum ?? null) ? $json->geburtsdatum : null;

        $mapping = [
            'comments' => 'vorgang.leadformular.kommentar',
            'tracking_id' => 'vorgang.tracking.tracking_id',
            'utm_campaign' => 'vorgang.tracking.utm_campaign',
            'utm_content' => 'vorgang.tracking.utm_content',
            'utm_medium' => 'vorgang.tracking.utm_medium',
            'utm_source' => 'vorgang.tracking.utm_source',
            'utm_term' => 'vorgang.tracking.utm_term',
            'agb' => 'vorgang.leadformular.agb_akzeptiert',
            'anrede' => 'vorgang.antragsteller.0.anrede',
            'vorname' => 'vorgang.antragsteller.0.vorname',
            'nachname' => 'vorgang.antragsteller.0.nachname',
            'geburtsdatum' => 'vorgang.antragsteller.0.geburtsdatum',
            'phone' => 'vorgang.antragsteller.0.telefonnummer',
            'email' => 'vorgang.antragsteller.0.email',
            'strasse' => 'vorgang.antragsteller.0.anschrift.strasse',
            'hausnummer' => 'vorgang.antragsteller.0.anschrift.hausnummer',
            'plz' => 'vorgang.antragsteller.0.anschrift.plz',
            'ort' => 'vorgang.antragsteller.0.anschrift.ort',
            'titel' => 'vorgang.antragsteller.0.titel',
        ];

        return $this->replace($json, $mapping);
    }

    /**
     * @param $item
     * @return array
     */
    private function mapApiValue($item)
    {
        $mapping = [
            'id' => 'vorgang.external_id',
            'europace_id' => 'vorgang.europace_id',

            'form_id' => 'vorgang.leadformular.id',
            'language' => 'vorgang.leadformular.sprache',
            'ref' => 'vorgang.leadformular.referer',

            'tracking_id' => 'vorgang.tracking.tracking_id',
            'utm_campaign' => 'vorgang.tracking.utm_campaign',
            'utm_content' => 'vorgang.tracking.utm_content',
            'utm_medium' => 'vorgang.tracking.utm_medium',
            'utm_source' => 'vorgang.tracking.utm_source',
            'utm_term' => 'vorgang.tracking.utm_term',
        ];
    }

    /**
     * @param $json
     * @return mixed
     */
    private function transformBasics($json)
    {
        $json->titel_prof = $json->titel_prof ?? null;
        $json->titel_dr = $json->titel_dr ?? null;

        if ($json->titel_prof != null && $json->titel_dr != null) {
            $json->titel = 'prof_dr';
        } else if ($json->titel_prof != null) {
            $json->titel = 'prof';
        } else if ($json->titel_dr != null) {
            $json->titel = 'dr';
        } else {
            $json->titel = null;
        }

        return $json;
    }

    private function setJobType($type)
    {
        switch ($type) {
            case 'ANGESTELLTER':
                return 'angestellter';

            case 'SELBSTSTAENDIGER':
                return 'selbstaendiger';

            case 'BEAMTER':
                return 'beamter';

            case 'RENTNER':
                return 'rentner';

            case 'SONSTIGES':
                return 'sonstiges';

            default:
                return '';
        }
    }

    private function replace($json, $mapping)
    {
        $return = [];
        $dot = new \Adbar\Dot($json);
        $json = $dot->flatten();

        foreach ($json as $key => $value) {
            if (key_exists($key, $mapping) && $value !== null && $value !== '') {
                if (preg_match('!^([.,0-9]+)$!', $value) && !preg_match('!([0-9]{2})\.([0-9]{2}).([0-9]{4})!', $value)) {
                    $value = str_replace('.', '', $value);
                    $value = str_replace(',', '.', $value);

                    // replace thousands separator
                    $value = preg_replace('!\.([0-9]{3})!', '$1', $value);
                }

                if($key === 'KUNDE1_EINKOMMEN_B'){
                    $return['vorgang.bonitaet.gehalt.0.antragsteller'] = 0;
                    $return['vorgang.boniteat.gehalt_anzahl'] = 1;
                }

                $return[$mapping[$key]] = $value;
            }

        }

        return $return;
    }
}
