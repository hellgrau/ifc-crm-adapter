<?php

namespace Hellgrau\Adapter\Jobs;

use Hellgrau\Adapter\Models\Order;
use Hellgrau\Adapter\PushService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PushOrder implements ShouldQueue
{
    /**
     * @var Order
     */
    protected $order;

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * SynchronizeOrder constructor.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     * @return void
     */
    public function handle()
    {
        PushService::sendOrder($this->order);
    }
}
