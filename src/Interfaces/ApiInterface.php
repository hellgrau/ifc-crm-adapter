<?php

namespace Hellgrau\Adapter\Interfaces;

interface ApiInterface
{
    /**
     * Returns API name
     * @return string
     */
    public function getName(): string;

    /**
     * Connects to the API
     * @return bool
     */
    public function login(): bool;

    /**
     * Requires and returns data for page X
     * @return array
     */
    public function getData(): array;

    /**
     * Convert the given item to defined structure (namespaces)
     * @param array $item
     * @return array
     */
    public function mapItem($item): array;
}
